package com.shanzhu.pay.demo.charge.consts;

/**
 * 交易类型
 * @author langkye
 */
public enum TradeType {
    //交易类型
    PAY,REFUND;
}
