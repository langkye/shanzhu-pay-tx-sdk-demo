package com.shanzhu.pay.demo.charge.service;

import com.shanzhu.pay.demo.charge.model.CreateChargeApiRequest;
import com.shanzhu.pay.demo.charge.model.NotifyResponse;
import com.shanzhu.pay.demo.charge.model.PayNotifyApiRequest;
import com.shanzhu.pay.demo.charge.model.QueryChargeApiRequest;
import com.shanzhu.pay.demo.common.exception.RRException;
import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.model.charge.*;
import com.shanzhu.pay.sdk.model.refund.CreateRefundRequest;
import com.shanzhu.pay.sdk.model.refund.CreateRefundResponse;
import com.shanzhu.pay.sdk.model.refund.QueryRefundRequest;
import com.shanzhu.pay.sdk.model.refund.QueryRefundResponse;

/**
 * 善筑SDK API
 *
 * @author langkye
 */
public interface ITxShanzhuSdkApiService {
    /**
     * 下单
     *
     * @param request       请求参数
     * @return              下单结果
     * @throws RRException  自定义异常
     */
    public CreateChargeResponse createOrder(CreateChargeApiRequest request) throws RRException;

    /**
     * 查询订单
     *
     * @param request       请求参数
     * @return              订单
     * @throws RRException  自定义异常
     */
    public QueryChargeResponse queryCharge(QueryChargeApiRequest request) throws RRException;

    /**
     * 关闭订单
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    public CloseChargeResponse closeCharge(CloseChargeRequest request) throws RRException;

    /**
     * 申请退款
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    public CreateRefundResponse refundCharge(CreateRefundRequest request) throws RRException;

    /**
     * 查询单笔退款
     *
     * @param request QueryRefundResponse
     * @return result
     * @throws RRException ex
     */
    public QueryRefundResponse queryRefund(QueryRefundRequest request) throws RRException;

    /**
     * 支付回调
     *
     * @param request       请求参数
     * @return              订单
     * @throws RRException  自定义异常
     */
    public NotifyResponse payNotify(PayNotifyApiRequest request) throws RRException;

    /**
     * 退款回调
     *
     * @param request       请求参数
     * @return              订单
     * @throws RRException  自定义异常
     */
    public NotifyResponse refundNotify(PayNotifyApiRequest request) throws RRException;

    /**
     * 构建[善筑SDK客户端]配置
     *
     * @param szMchId       善筑侧商户编号
     * @return              [善筑SDK客户端]配置
     * @throws RRException  自定义异常
     */
    public DefaultShanZhuPayClient.Config buildDefaultShanZhuPayClientConfig(String szMchId) throws RRException;
}
