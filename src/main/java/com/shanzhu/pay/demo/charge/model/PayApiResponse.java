package com.shanzhu.pay.demo.charge.model;

import com.alibaba.fastjson.JSONObject;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author langkye
 * @date 2021/9/9
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors
public class PayApiResponse<T> extends JSONObject {
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
