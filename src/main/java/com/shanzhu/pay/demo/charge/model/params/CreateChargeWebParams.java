package com.shanzhu.pay.demo.charge.model.params;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 页面下单参数
 *
 * @author langkye
 * @date 2021/9/9
 */
@Data
public class CreateChargeWebParams {
    /**下单金额*/
    @NotNull(message = "支付金额不能为空")
    private BigDecimal amount;
    /**商品名称*/
    @NotNull(message = "商品名称不能为空")
    private String shopName;

    /**appId*/
    private String appId;

    /**openId*/
    private String appUserId;

    @NotBlank(message = "支付渠道类型不能为空")
    private String channelType;

    private Map<String, Object> extra;

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
