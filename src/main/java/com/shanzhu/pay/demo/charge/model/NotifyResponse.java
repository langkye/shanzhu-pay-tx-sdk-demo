package com.shanzhu.pay.demo.charge.model;

import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.charge.model.params.NotifyBizData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 回调响应
 *
 * @author langkye
 * @date 2021/10/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors
@Builder
public class NotifyResponse implements Serializable {
    private static final Long serialVersionUID = 1L;

    private boolean bizException = false;
    private String clientCode = "";
    private String clientMessage = "";

    private NotifyBizData notifyBizData = NotifyBizData.builder().build();

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
