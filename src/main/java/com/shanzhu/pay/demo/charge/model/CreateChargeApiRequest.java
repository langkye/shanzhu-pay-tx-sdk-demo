package com.shanzhu.pay.demo.charge.model;

import com.shanzhu.pay.demo.charge.consts.Channel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author langkye
 * @date 2021/9/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors
public class CreateChargeApiRequest {
    /**订单总金额，单位为分。*/
    private Integer amount;
    /**商品标题，不可使用特殊字符。*/
    private String subject;
    /**用户标识:小程序openId*/
    private String appUserId;
    /**请求流水号：客户端保证唯一*/
    private String requestSerialNo;
    /**支付属性*/
    private Channel channelType;
    //etc...
    private Map<String, Object> extra;
}
