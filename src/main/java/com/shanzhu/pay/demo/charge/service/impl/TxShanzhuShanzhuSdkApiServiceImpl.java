package com.shanzhu.pay.demo.charge.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.charge.consts.NotifyType;
import com.shanzhu.pay.demo.charge.model.*;
import com.shanzhu.pay.demo.charge.model.params.NotifyBizData;
import com.shanzhu.pay.demo.charge.service.ITxShanzhuSdkApiService;
import com.shanzhu.pay.demo.common.exception.RRException;
import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.exception.InvalidNotificationException;
import com.shanzhu.pay.sdk.model.charge.*;
import com.shanzhu.pay.sdk.model.charge.CloseChargeRequest;
import com.shanzhu.pay.sdk.model.refund.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author langkye
 * @date 2021/9/7
 */
@Service
public class TxShanzhuShanzhuSdkApiServiceImpl implements ITxShanzhuSdkApiService {
    private static final Logger logger = LoggerFactory.getLogger(TxShanzhuShanzhuSdkApiServiceImpl.class);

    @Value("${wx.appid}") private String appId;
    @Value("${client.sz.chl.serviceUrl}") private String serverUrl;
    @Value("${client.sz.chl.publicKey}") private String szPublicKey;
    @Value("${client.sz.self.mchId}") private String mchId;
    @Value("${client.sz.self.privateKey}") private String privateKey;
    @Value("${client.sz.self.keySerialNo}") private String publicKey;
    @Value("${client.sz.self.apiKey}") private String apiKey;
    @Value("${client.sz.self.keySerialNo}") private String keySerialNo;
    @Value("${client.sz.pay.notifyUrl}") private String payNotifyUrl;

    /**
     * 下单
     *
     * @param request 请求参数
     * @return 支付结果
     * @throws RRException 自定义异常
     */
    @Override
    public CreateChargeResponse createOrder(CreateChargeApiRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("下单失败：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("下单失败：{}", ex.getMessage());
        //    throw new RRException("下单失败：参数校验失败");
        //}

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        //构建下单请求参数
        CreateChargeRequest createChargeRequest = new CreateChargeRequest();
        //-------必填参数
        // 收款方善筑侧商户编号(必选)
        createChargeRequest.setMchId(mchId);
        // 支付使用的第三方支付渠道(必选)
        createChargeRequest.setChannel(request.getChannelType().getCode());
        // 商品标题(必选)
        createChargeRequest.setSubject(request.getSubject());
        // 商户订单号(必选)
        createChargeRequest.setOutOrderNo(request.getRequestSerialNo());
        // 订单总金额，单位为分。(必选)
        createChargeRequest.setAmount(request.getAmount());
        //-------条件参数
        // 商户微信公众号或小程序appId
        createChargeRequest.setAppId(appId);
        // 通知地址,必须为直接可访问的URL，不允许携带查询串。
        createChargeRequest.setNotifyUrl(payNotifyUrl);
        // 用户标识
        createChargeRequest.setAppUserId(request.getAppUserId());
        // 货币类型，CNY：人民币，境内商户号仅支持人民币。
        createChargeRequest.setCurrency("CNY");
        // 订单失效时间，单位是分钟，从创建订单开始计时。
        //createChargeRequest.setExpiresIn(10);

        //--额外参数

        Map<String, Object> extra = request.getExtra();
        extra = extra == null ? new HashMap<String, Object>(): extra;
        //交易分账标识：0-普通交易；1-分账；2-交易主体分账（延迟结算）
        //extra.put("royalty", "0");
        //扫码
        //extra.put("auth_code", "0");
        //etc...
        createChargeRequest.setExtra(extra);

        /*发起请求*/
        logger.info("###req:{}", JSONObject.toJSONString(createChargeRequest));
        CreateChargeResponse response = shanZhuPayClient.createCharge(createChargeRequest);
        logger.info("###resp:{}", JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
            throw new RRException("调用失败，服务不可用，请重试:" + response.getMsg());
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getMsg());
            throw new RRException("调用失败，业务错误: " + response.getMsg());
        }
        return response;
    }

    /**
     * 查询订单
     *
     * @param request       请求参数
     * @return              订单
     * @throws RRException  自定义异常
     */
    @Override
    public QueryChargeResponse queryCharge(QueryChargeApiRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        QueryChargeRequest queryChargeRequest = new QueryChargeRequest();
        // 善筑支付订单号：下单返回
        queryChargeRequest.setSzOrderNo(request.getSzOrderNo());
        queryChargeRequest.setOutOrderNo(request.getOutOrderNo());
        queryChargeRequest.setExtra(request.getExtra());

        QueryChargeResponse response = shanZhuPayClient.queryCharge(queryChargeRequest);

        logger.info("###resp:{}", JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
            throw new RRException("调用失败，服务不可用，请重试:" + response.getMsg());
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getMsg());
            throw new RRException("调用失败，业务错误: " + response.getMsg());
        }
        return response;
    }

    /**
     * 关闭订单
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public CloseChargeResponse closeCharge(CloseChargeRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        // 善筑支付订单号：下单返回
        request.setMchId(mchId);

        CloseChargeResponse response = shanZhuPayClient.closeCharge(request);

        logger.info("###resp:{}", JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
            throw new RRException("调用失败，服务不可用，请重试:" + response.getMsg());
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getMsg());
            throw new RRException("调用失败，业务错误: " + response.getMsg());
        }
        return response;
    }

    /**
     * 申请退款
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public CreateRefundResponse refundCharge(CreateRefundRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);

        CreateRefundResponse response = shanZhuPayClient.createRefund(request);

        logger.info("###resp:{}", JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
            throw new RRException("调用失败，服务不可用，请重试:" + response.getMsg());
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getMsg());
            throw new RRException("调用失败，业务错误: " + response.getMsg());
        }
        return response;
    }

    /**
     * 查询单笔退款
     *
     * @param request QueryRefundRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public QueryRefundResponse queryRefund(QueryRefundRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);

        QueryRefundResponse response = shanZhuPayClient.queryRefund(request);

        logger.info("###resp:{}", JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
            throw new RRException("调用失败，服务不可用，请重试:" + response.getMsg());
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getMsg());
            throw new RRException("调用失败，业务错误: " + response.getMsg());
        }
        return response;
    }

    /**
     * 支付回调
     *
     * @param request 请求参数
     * @return 订单
     * @throws RRException 自定义异常
     */
    @Override
    public NotifyResponse payNotify(PayNotifyApiRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("支付回调：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("支付回调：{}", ex.getMessage());
        //    throw new RRException("支付回调：参数校验失败");
        //}

        /*响应信息*/
        final NotifyResponse response = new NotifyResponse();

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);

        //解析数据体
        try {
            final ChargeNotificationData chargeNotificationData = shanZhuPayClient
                    .checkAndParseChargeNotification(
                            request.getMethod(),
                            request.getRequestUrl(),
                            request.getBody(),
                            request.getAuthorization());
            //创建订单时的商户订单号
            final String outOrderNo = chargeNotificationData.getOutOrderNo();
            //支付状态：
            //      SUCCESS-支付成功；
            //      NOT_PAY-未支付；
            //      CLOSED-已关闭；
            //      ACCEPT-已接收，等待扣款；
            //      USER_PAYING-用户支付中；
            //      PAY_FAILED-支付失败
            final String status = chargeNotificationData.getStatus();

            // TODO 这里调用你的业务逻辑，设置合理的状态
            boolean success = true;

            /*构建响应信息*/
            if(success){
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.SUCCESS)
                                .msg(NotifyType.SUCCESS.getMsg())
                                .build());
            } else {
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.FAILED)
                                .msg(NotifyType.FAILED.getMsg())
                                .build());
            }
        } catch (InvalidNotificationException ex) {
            logger.error("校验通知失败:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("INVALID_NOTIFICATION");
            response.setClientMessage("无效的回调通知:" + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            logger.error("非法的参数:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("ILLEGAL_ARGUMENT");
            response.setClientMessage("参数异常:" + ex.getMessage());
        } catch (Exception ex) {
            logger.error("未知异常：{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("OTHER_ERROR");
            response.setClientMessage("未知异常，回调处理失败:" + ex.getMessage());
        }
        return response;
    }

    /**
     * 退款回调
     *
     * @param request 请求参数
     * @return 订单
     * @throws RRException 自定义异常
     */
    @Override
    public NotifyResponse refundNotify(PayNotifyApiRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(request);
        //} catch (RRException ex) {
        //    logger.error("支付回调：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("支付回调：{}", ex.getMessage());
        //    throw new RRException("支付回调：参数校验失败");
        //}

        /*响应信息*/
        final NotifyResponse response = new NotifyResponse();

        /*1/：创建客户端配置*/
        final DefaultShanZhuPayClient.Config config = this.buildDefaultShanZhuPayClientConfig(mchId);

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);

        //解析数据体
        try {
            final RefundNotificationData chargeNotificationData = shanZhuPayClient
                    .checkAndParseRefundNotification(
                            request.getMethod(),
                            request.getRequestUrl(),
                            request.getBody(),
                            request.getAuthorization()
                    );
            //创建订单时的商户订单号
            final String outOrderNo = chargeNotificationData.getOutOrderNo();
            //退款状态：
            //      SUCCESS—退款成功
            //      CLOSED—退款关闭
            //      PROCESSING—退款处理中
            //      FAILED—退款异常
            final String status = chargeNotificationData.getStatus();

            // TODO 这里调用你的业务逻辑，设置合理的状态
            boolean success = true;

            /*构建响应信息*/
            if(success){
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.SUCCESS)
                                .msg(NotifyType.SUCCESS.getMsg())
                                .build());
            } else {
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.FAILED)
                                .msg(NotifyType.FAILED.getMsg())
                                .build());
            }
        } catch (InvalidNotificationException ex) {
            logger.error("校验通知失败:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("INVALID_NOTIFICATION");
            response.setClientMessage("无效的回调通知:" + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            logger.error("非法的参数:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("ILLEGAL_ARGUMENT");
            response.setClientMessage("参数异常:" + ex.getMessage());
        } catch (Exception ex) {
            logger.error("未知异常：{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("OTHER_ERROR");
            response.setClientMessage("未知异常，回调处理失败:" + ex.getMessage());
        }
        return response;
    }

    /**
     * 构建[善筑SDK客户端]配置
     *
     * @param szMchId       善筑侧商户编号
     * @return              [善筑SDK客户端]配置
     * @throws RRException  自定义异常
     */
    @Override
    public DefaultShanZhuPayClient.Config buildDefaultShanZhuPayClientConfig(String szMchId) throws RRException{
        //参数校验
        if (StringUtils.isBlank(szMchId)) {
            throw new RRException("善筑侧商户编号不能为空");
        }

        /*开始构建[善筑SDK客户端]配置*/
        DefaultShanZhuPayClient.Config config = new DefaultShanZhuPayClient.Config();
        //API接口服务地址
        config.setServerUrl(serverUrl);
        //商户号：善筑下发
        config.setMchId(szMchId);
        //商户私钥：善筑下发
        config.setPrivateKey(privateKey);
        //善筑平台公钥：善筑下发
        config.setShanzhuPublicKey(szPublicKey);
        //商户API秘钥：善筑下发
        config.setApiKey(apiKey);
        //秘钥序列号：善筑下发
        config.setKeySerialNo(keySerialNo);

        return config;
    }
}
