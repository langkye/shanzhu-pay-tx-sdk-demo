package com.shanzhu.pay.demo.charge.consts;

/**
 * 订单状态
 *
 * @author langkye
 */
public enum OrderStatus {
    /**订单状态枚举*/
    SUCCESS("SUCCESS", "支付成功"),
    NOT_PAY("NOT_PAY", "未支付"),
    CLOSED("CLOSED", "已关闭"),
    USERPAYING("USERPAYING", "用户支付中"),
    PAY_FAILED("PAY_FAILED", "支付失败"),
    ;

    /**订单状态编码*/
    private final String code;
    /**订单状态描述*/
    private final String desc;

    /**
     * 私有构造
     *
     * @param code 支付渠道类型编码
     * @param desc 支付渠道类型描述
     */
    private OrderStatus(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 获取支付渠道类型编码
     *
     * @return 支付渠道类型编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 获取支付渠道类型描述
     *
     * @return 支付渠道类型描述
     */
    public String getDesc() {
        return desc;
    }
}
