package com.shanzhu.pay.demo.charge.controller;

import com.alibaba.fastjson.JSONObject;

import com.shanzhu.pay.demo.charge.model.NotifyResponse;
import com.shanzhu.pay.demo.charge.model.params.CreateChargeWebParams;
import com.shanzhu.pay.demo.charge.model.params.QueryChargeWebParams;
import com.shanzhu.pay.demo.charge.service.IApiPayService;
import com.shanzhu.pay.demo.common.exception.RRException;
import com.shanzhu.pay.sdk.model.charge.CloseChargeRequest;
import com.shanzhu.pay.sdk.model.charge.CloseChargeResponse;
import com.shanzhu.pay.sdk.model.charge.CreateChargeResponse;
import com.shanzhu.pay.sdk.model.charge.QueryChargeResponse;
import com.shanzhu.pay.sdk.model.refund.CreateRefundRequest;
import com.shanzhu.pay.sdk.model.refund.CreateRefundResponse;
import com.shanzhu.pay.sdk.model.refund.QueryRefundRequest;
import com.shanzhu.pay.sdk.model.refund.QueryRefundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author langkye
 * @date 2021/9/9
 */
@RestController
@RequestMapping("/")
public class PayApiController {
    private static final Logger logger = LoggerFactory.getLogger(PayApiController.class);

    @Autowired private IApiPayService apiPayService;

    /**
     * 下单
     * json参数示例：
     * --小程序/微信公众号：
     * {
     *     "amount": 0.01,
     *     "appUserId": "oKDWF4k8LHc-ZLrgDfLUvQ78cBiE",
     *     "channelType": "WX_JSAPI",
     *     "shopName": "商铺"
     * }
     * --扫码支付：
     * {
     *     "amount": 0.01,
     *     "channelType": "SCAN",
     *     "shopName": "二维码支付"
     *     ,"extra": {
     *         "auth_code": "2854 8695 6408 781215"
     *     }
     * }
     *
     * @param params  params
     * @return result
     */
    @PostMapping("/api/szszfin/createCharge")
    public Object createCharge(@RequestBody CreateChargeWebParams params) {
        logger.info("request body: {}", params);
        final CreateChargeResponse data = apiPayService.createCharge(params);

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data.getBizData());

        return result;
    }

    /**
     * 查询订单
     * json参数示例：
     *
     * {
     *     "szOrderNo": "71513202751275008",
     *     "outOrderNo":"",
     *     "extra":{}
     * }
     *
     * @param params  params
     * @return result
     */
    @PostMapping("/api/szszfin/queryCharge")
    public Object queryCharge(@RequestBody QueryChargeWebParams params) {
        logger.info("request body: {}", params);
        final QueryChargeResponse data = apiPayService.queryCharge(params);

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data.getBizData());

        return result;
    }

    /**
     * 关闭订单
     * json 示例
     * {
     *   "mch_id": "1230000109",
     *   "sz_order_no": "70501111111S001111119",
     * }
     *
     * @param request CloseChargeRequest
     * @return result
     */
    @PostMapping("/api/szszfin/closeCharge")
    public Object closeCharge(@RequestBody CloseChargeRequest request) {
        logger.info("request body: {}", request);

        final CloseChargeResponse data = apiPayService.closeCharge(request);

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data.getBizData());

        return result;
    }

    /**
     * 申请退款
     * json 示例
     * {
     *   "code": "10000",
     *   "bizData": {
     *     "sz_refund_no": "90501111111S001111119",
     *     "channel_refund_no": "2007752501201407033233368018",
     *     "extra": {
     *       "settlement_refund_fee": 100,
     *       "cash_fee": 100,
     *       "cash_fee_type": "CNY",
     *       "cash_refund_fee": 100,
     *       "coupon_refund_fee": 100,
     *       "coupon_refund_count": 1
     *     }
     *   }
     * }
     *
     * @param request CloseChargeRequest
     * @return result
     */
    @PostMapping("/api/szszfin/refundCharge")
    public Object refundCharge(@RequestBody CreateRefundRequest request) {
        logger.info("request body: {}", request);

        final CreateRefundResponse data = apiPayService.refundCharge(request);

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data.getBizData());

        return result;
    }

    /**
     * 查询单笔退款
     * json 示例:
     * {
     *   "mch_id": "1230000109",
     *   "sz_order_no": "70501111111S001111119",
     *   "sz_refund_order_no": "1217752501201407033233368018"
     * }
     *
     * @param request CloseChargeRequest
     * @return result
     */
    @PostMapping("/api/szszfin/queryRefund")
    public Object queryRefund(@RequestBody QueryRefundRequest request) {
        logger.info("request body: {}", request);

        final QueryRefundResponse data = apiPayService.queryRefund(request);

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data.getBizData());

        return result;
    }

    /**
     * 支付回调
     *
     * @param httpServletRequest  params
     * @return result
     */
    @RequestMapping("/api/szszfin/payNotify")
    public Object payNotify(HttpServletRequest httpServletRequest) {
        logger.info("###善筑支付回调");
        final NotifyResponse response = apiPayService.payNotify(httpServletRequest);

        //如果有业务异常
        if (response.isBizException()) {
            throw new RRException(
                    response.getClientMessage()
                    , 500);
        }

        logger.info("回调处理结果: {}。详细信息：{}", response.getNotifyBizData().getCode(), response);

        return response.getNotifyBizData();
    }

    /**
     * 退款回调
     *
     * @param httpServletRequest  params
     * @return result
     */
    public Object refundNotify(HttpServletRequest httpServletRequest) {
        logger.info("###善筑退款回调");
        final NotifyResponse response = apiPayService.refundNotify(httpServletRequest);

        //如果有业务异常
        if (response.isBizException()) {
            throw new RRException(
                    response.getClientMessage()
                    , 500);
        }

        logger.info("回调处理结果: {}。详细信息：{}", response.getNotifyBizData().getCode(), response);

        return response.getNotifyBizData();
    }
}
