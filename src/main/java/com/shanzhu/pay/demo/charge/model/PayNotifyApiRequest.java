package com.shanzhu.pay.demo.charge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 支付结果回调
 *
 * @author langkye
 * @date 2021/9/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors
public class PayNotifyApiRequest {
    private String requestUrl;
    private String method;
    private String authorization;
    private String body;
}
