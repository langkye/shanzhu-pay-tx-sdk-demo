package com.shanzhu.pay.demo.charge.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 关闭订单
 *
 * @author langkye
 * @date 2021/10/14
 */
@Data
public class CloseChargeRequest {
    /**商户号，商户在善筑支付平台进件的商户号，由善筑支付生成并下发。*/
    @NotBlank(message = "商户编号[mchId]不能为空")
    private String mchId;
    /**二选一：创建订单时返回的善筑支付订单号，建议优先使用*/
    private String szOrderNo;
    /**二选一：创建订单时的商户订单号*/
    private String outOrderNo;
}
