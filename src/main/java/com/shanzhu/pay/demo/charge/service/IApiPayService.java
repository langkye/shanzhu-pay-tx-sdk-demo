package com.shanzhu.pay.demo.charge.service;

import com.shanzhu.pay.demo.charge.model.NotifyResponse;
import com.shanzhu.pay.demo.charge.model.params.CreateChargeWebParams;
import com.shanzhu.pay.demo.charge.model.params.QueryChargeWebParams;
import com.shanzhu.pay.demo.common.exception.RRException;
import com.shanzhu.pay.sdk.model.charge.CloseChargeRequest;
import com.shanzhu.pay.sdk.model.charge.CloseChargeResponse;
import com.shanzhu.pay.sdk.model.charge.CreateChargeResponse;
import com.shanzhu.pay.sdk.model.charge.QueryChargeResponse;
import com.shanzhu.pay.sdk.model.refund.CreateRefundRequest;
import com.shanzhu.pay.sdk.model.refund.CreateRefundResponse;
import com.shanzhu.pay.sdk.model.refund.QueryRefundRequest;
import com.shanzhu.pay.sdk.model.refund.QueryRefundResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * API Service
 * 接口文档：https://www.yuque.com/ycp2i6/yffxqb
 *
 * @author langkye
 */
public interface IApiPayService {
    /** 统一下单
     *  文档：https://www.yuque.com/ycp2i6/yffxqb/xt7uld
     *
     * @param <T>       响应参数类型
     * @param params    请求参数
     * @return          响应数据
     */
    public <T> CreateChargeResponse createCharge(CreateChargeWebParams params) ;

    /**
     * 查询订单
     *
     * @param <T>       响应参数类型
     * @param params    请求参数
     * @return          响应数据
     */
    public <T> QueryChargeResponse queryCharge(QueryChargeWebParams params) ;

    /**
     * 关闭订单
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    public <T> CloseChargeResponse closeCharge(CloseChargeRequest request) throws RRException;

    /**
     * 申请退款
     *
     * @param request CreateRefundRequest
     * @return result
     * @throws RRException ex
     */
    public <T> CreateRefundResponse refundCharge(CreateRefundRequest request) throws RRException;

    /**
     * 查询单笔退款
     *
     * @param request QueryRefundRequest
     * @return result
     * @throws RRException ex
     */
    public QueryRefundResponse queryRefund(QueryRefundRequest request) throws RRException;

    /**
     * 支付回调
     *
     * @param httpServletRequest httpServletRequest
     * @return 响应结果
     * @throws RRException ex
     */
    public NotifyResponse payNotify(HttpServletRequest httpServletRequest) throws RRException;

    /**
     * 退款回调
     *
     * @param httpServletRequest httpServletRequest
     * @return 响应结果
     * @throws RRException ex
     */
    public NotifyResponse refundNotify(HttpServletRequest httpServletRequest) throws RRException;

}
