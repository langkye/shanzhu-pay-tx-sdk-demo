package com.shanzhu.pay.demo.charge.consts;

/**
 * 支付渠道属性值
 *
 * @doc https://www.yuque.com/ycp2i6/yffxqb/yv78et
 * @author langkye
 */
public enum Channel {
    /**支付渠道属性值枚举*/
    WX("WX", "微信App支付"),
    WX_JSAPI("WX_JSAPI", "微信公众号或小程序支付"),
    WX_PUB_QR("WX_PUB_QR", "微信Native支付"),
    WX_PUB_SCAN("WX_PUB_SCAN", "微信付款码支付"),
    WX_WAP("WX_WAP", "微信H5支付"),
    ALIPAY("ALIPAY", "支付宝App支付"),
    ALIPAY_WAP("ALIPAY_WAP", "支付宝手机网站支付"),
    ALIPAY_QR("ALIPAY_QR", "支付宝扫码支付"),
    ALIPAY_SCAN("ALIPAY_SCAN", "支付宝条码支付"),
    ALIPAY_LITE("ALIPAY_LITE", "支付宝小程序支付"),
    ALIPAY_PC_DIRECT("ALIPAY_PC_DIRECT", "支付宝电脑网站支付"),
    UPACP_QR("UPACP_QR", "银联扫码支付"),
    UPACP_SCAN("UPACP_SCAN", "银联条码支付"),
    QR("QR", "聚合码"),
    SCAN("SCAN", "二维码支付"),
    ;

    /**支付渠道属性值*/
    private final String code;
    /**支付渠道名称*/
    private final String desc;

    /**
     * 私有构造
     *
     * @param code 支付渠道类型编码
     * @param desc 支付渠道类型描述
     */
    private Channel(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 获取支付渠道类型编码
     *
     * @return 支付渠道类型编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 获取支付渠道类型描述
     *
     * @return 支付渠道类型描述
     */
    public String getDesc() {
        return desc;
    }
}
