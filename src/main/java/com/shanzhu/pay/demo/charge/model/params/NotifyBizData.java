package com.shanzhu.pay.demo.charge.model.params;

import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.charge.consts.NotifyType;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 回调业务数据
 *
 * @author langkye
 * @date 2021/10/14
 */
@EqualsAndHashCode(callSuper = true)
@Data
//@AllArgsConstructor
@NoArgsConstructor
@Accessors
@Builder
public class NotifyBizData extends JSONObject implements Serializable {
    private static final Long serialVersionUID = 1L;

    private NotifyType code;
    private String msg = "";

    NotifyBizData(NotifyType code) {
        this.code = code;
        this.put("code", this.code);
        this.put("msg", code.getMsg());
    }

    NotifyBizData(String msg) {
        this.msg = msg;
        this.put("msg", msg);
    }

    NotifyBizData(NotifyType code, String msg) {
        this.msg = msg == null ? "" : msg;
        this.code = code;
        this.put("code", this.code);
        this.put("msg", this.msg);
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
