package com.shanzhu.pay.demo.charge.model;

import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.charge.consts.TradeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * 回调请求
 *
 * @author langkye
 * @date 2021/10/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors
@Builder
public class NotifyRequest {
    @NotNull(message = "交易类型[tradeType]不能为空")
    private TradeType tradeType;
    @NotNull(message = "请求[httpServletRequest]不能为空")
    private HttpServletRequest httpServletRequest;
}
