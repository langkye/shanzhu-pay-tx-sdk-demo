package com.shanzhu.pay.demo.charge.service.impl;

import com.devskiller.friendly_id.FriendlyId;
import com.shanzhu.pay.demo.charge.consts.Channel;
import com.shanzhu.pay.demo.charge.consts.TradeType;
import com.shanzhu.pay.demo.charge.model.*;
import com.shanzhu.pay.demo.charge.model.params.CreateChargeWebParams;
import com.shanzhu.pay.demo.charge.model.params.QueryChargeWebParams;
import com.shanzhu.pay.demo.charge.service.IApiPayService;
import com.shanzhu.pay.demo.charge.service.ITxShanzhuSdkApiService;
import com.shanzhu.pay.demo.common.exception.RRException;
import com.shanzhu.pay.sdk.model.charge.CloseChargeRequest;
import com.shanzhu.pay.sdk.model.charge.CloseChargeResponse;
import com.shanzhu.pay.sdk.model.charge.CreateChargeResponse;
import com.shanzhu.pay.sdk.model.charge.QueryChargeResponse;
import com.shanzhu.pay.sdk.model.refund.CreateRefundRequest;
import com.shanzhu.pay.sdk.model.refund.CreateRefundResponse;
import com.shanzhu.pay.sdk.model.refund.QueryRefundRequest;
import com.shanzhu.pay.sdk.model.refund.QueryRefundResponse;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;

/**
 * API Service
 * 接口文档：https://www.yuque.com/ycp2i6/yffxqb
 *
 * @author langkye
 * @date 2021/9/9
 */
@Service
public class ApiPayServiceImpl implements IApiPayService {
    private static final Logger logger = LoggerFactory.getLogger(ApiPayServiceImpl.class);
    @Autowired private ITxShanzhuSdkApiService txShanzhuSdkApiService;

    /**
     * 统一下单
     * 文档：https://www.yuque.com/ycp2i6/yffxqb/xt7uld
     *
     * @param params    请求参数
     * @return          响应数据
     */
    @SuppressWarnings(value = {"unchecked"})
    @Override
    public CreateChargeResponse createCharge(CreateChargeWebParams params) {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(params);
        //} catch (RRException ex) {
        //    logger.error("下单失败：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("下单失败：{}", ex.getMessage());
        //    throw new RRException("下单失败：参数校验失败");
        //}

        return txShanzhuSdkApiService.createOrder(
                CreateChargeApiRequest
                        .builder()
                        .amount(params.getAmount().multiply(BigDecimal.valueOf(100)).intValue())
                        .appUserId(params.getAppUserId())
                        .channelType(Channel.valueOf(params.getChannelType()))
                        .requestSerialNo(FriendlyId.createFriendlyId())
                        .extra(params.getExtra())
                        .subject(params.getShopName())
                        .build()
        );
    }

    /**
     * 查询订单
     *
     * @param <T>       响应参数类型
     * @param params    请求参数
     * @return          响应数据
     */
    @Override
    public <T> QueryChargeResponse queryCharge(QueryChargeWebParams params) {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(params);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        return txShanzhuSdkApiService.queryCharge(QueryChargeApiRequest.builder()
                .szOrderNo(params.getSzOrderNo())
                .outOrderNo(params.getOutOrderNo())
                .extra(params.getExtra())
                .build());
    }

    /**
     * 关闭订单
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public CloseChargeResponse closeCharge(CloseChargeRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(params);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        return txShanzhuSdkApiService.closeCharge(request);
    }

    /**
     * 申请退款
     *
     * @param request CloseChargeRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public <T> CreateRefundResponse refundCharge(CreateRefundRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(params);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        return txShanzhuSdkApiService.refundCharge(request);
    }

    /**
     * 查询单笔退款
     *
     * @param request QueryRefundRequest
     * @return result
     * @throws RRException ex
     */
    @Override
    public QueryRefundResponse queryRefund(QueryRefundRequest request) throws RRException {
        /*0/：参数校验*/
        //try {
        //    ValidatorUtils.validateEntity(params);
        //} catch (RRException ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw ex;
        //} catch (Exception ex) {
        //    logger.error("查询订单：{}", ex.getMessage());
        //    throw new RRException("查询订单：参数校验失败");
        //}

        return txShanzhuSdkApiService.queryRefund(request);
    }

    /**
     * 支付回调
     *
     * @param httpServletRequest httpServletRequest
     * @return 响应结果
     * @throws RRException ex
     */
    @Override
    public NotifyResponse payNotify(HttpServletRequest httpServletRequest) throws RRException {
        return notify(
                NotifyRequest.builder()
                        .tradeType(TradeType.PAY)
                        .httpServletRequest(httpServletRequest)
                        .build());
    }

    /**
     * 退款回调
     *
     * @param httpServletRequest httpServletRequest
     * @return 响应结果
     * @throws RRException ex
     */
    @Override
    public NotifyResponse refundNotify(HttpServletRequest httpServletRequest) throws RRException {
        return notify(
                NotifyRequest.builder()
                        .tradeType(TradeType.REFUND)
                        .httpServletRequest(httpServletRequest)
                        .build());
    }

    public NotifyResponse notify(NotifyRequest notifyRequest) throws RRException {
        final HttpServletRequest httpServletRequest = notifyRequest.getHttpServletRequest();

        /*响应信息*/
        final NotifyResponse response = new NotifyResponse();

        /*从请求头获取参数*/
        final String authorization = httpServletRequest.getHeader("Authorization");
        final StringBuffer requestUrl = httpServletRequest.getRequestURL();
        final String method = httpServletRequest.getMethod();

        //解析参数：body
        String body = "";
        try {
            final InputStream inputStream = httpServletRequest.getInputStream();
            body = IOUtils.toString(inputStream);
            logger.info("###authorization: {}", authorization);
            logger.info("###body: {}", body);
            logger.info("###requestURL: {}", requestUrl.toString());
            logger.info("###method: {}", method);
        } catch (Exception ex) {
            logger.error("获取参数失败：{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            String message = "解析回调请求参数失败:" + ex.getMessage();
            response.setBizException(true);
            response.setClientCode("OTHER_ERROR");
            response.setClientMessage(message);
            return response;
        }

        final TradeType tradeType = notifyRequest.getTradeType();
        if (TradeType.PAY.equals(tradeType)) {
            return txShanzhuSdkApiService.payNotify(PayNotifyApiRequest.builder()
                    .method(method)
                    .requestUrl(requestUrl.toString())
                    .body(body)
                    .authorization(authorization)
                    .build());
        }
        else if (TradeType.REFUND.equals(tradeType)) {
            return txShanzhuSdkApiService.refundNotify(PayNotifyApiRequest.builder()
                    .method(method)
                    .requestUrl(requestUrl.toString())
                    .body(body)
                    .authorization(authorization)
                    .build());
        }
        else {
            String message = "不支持的交易类型:" + tradeType;
            response.setBizException(true);
            response.setClientCode("TRADE_TYPE_ERROR");
            response.setClientMessage(message);
            return response;
        }
    }

}
