package com.shanzhu.pay.demo.charge.model;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author langkye
 * @date 2021/9/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors
public class QueryChargeApiRequest {
    /**创建订单时返回的善筑支付订单号，建议优先使用(二选一)*/
    private String szOrderNo;
    /**创建订单时的商户订单号(二选一)*/
    private String outOrderNo;

    /**特定渠道发起交易时需要的额外参数，详细参考*/
    private JSONObject extra;
}
