package com.shanzhu.pay.demo.charge.consts;

import lombok.Getter;

/**
 * 回调类型
 * @author langkye
 */
@Getter
public enum NotifyType {
    /**回调类型*/
    FAILED("FAILED", "失败")
    ,SUCCESS("SUCCESS", "成功")
    ;

    private final String code;
    private final String msg;

    private NotifyType(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
