package com.shanzhu.pay.demo.applets.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.api.impl.WxMaUserServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.common.util.PropertyUtil;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信小程序
 *
 * @author langkye
 */
@RestController
@RequestMapping("/")
@SuppressWarnings(value = {"ALL"})
public class WeixinApp {
    private static final String appId = PropertyUtil.getValue("wx.appid");
    private static final String appSecret = PropertyUtil.getValue("wx.appSecret");

    private final WxMaUserService wxMaUserService;
    private final WxMpServiceImpl wxMpService;

    /**
     * 小程序登陆
     *
     * @param request           请求参数
     * @return                  WxMaJscode2SessionResult
     * @throws WxErrorException WxErrorException
     */
    @PostMapping(value = "/api/base/tx/user/login", produces = "application/json;charset=UTF-8")
    public JSONObject txLogin(@RequestBody JSONObject request) throws WxErrorException {
        String jsCode = request.getString("jsCode");

        WxMaJscode2SessionResult sessionInfo = wxMaUserService.getSessionInfo(jsCode);

        JSONObject data = new JSONObject();
        data.put("userId", sessionInfo.getOpenid());
        data.put("unionId", sessionInfo.getUnionid());
        data.put("sessionKey", sessionInfo.getSessionKey());

        final JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        result.put("content", data);

        return result;
    }

    /**
     * 获取openId
     *
     * @param request           请求参数
     * @return                  WxMaJscode2SessionResult
     * @throws WxErrorException WxErrorException
     */
    @PostMapping(value = "/api/base/tx/user/fetchOpenId", produces = "application/json;charset=UTF-8")
    public JSONObject fetchOpenId(@RequestBody JSONObject request){
        //获取[oauth code]
        String code = request.getString("code");
        final String openId;

        //返回结果对象
        final JSONObject result = new JSONObject();

        try {
            //获取openId
            openId = wxMpService.getOAuth2Service().getAccessToken(code).getOpenId();
        } catch (WxErrorException ex) {
            result.put("code", "500");
            result.put("success", "0");
            result.put("message", "获取openId失败:" + ex.getMessage());
            return result;
        }

        result.put("code", "200");
        result.put("success", "1");
        result.put("message", "操作成功");
        final JSONObject content = new JSONObject();
        content.put("openId", openId);

        result.put("content", content);

        return result;
    }

    /**
     * 获取手机号
     *
     * @param request           请求参数
     * @return                  WxMaPhoneNumberInfo
     * @throws WxErrorException WxErrorException
     */
    @PostMapping(value = "/api/base/tx/user/getPhoneNumber", produces = "application/json;charset=UTF-8")
    public WxMaPhoneNumberInfo getPhoneNumber(@RequestBody JSONObject request) throws WxErrorException {
        return wxMaUserService.getPhoneNoInfo(
                request.getString("sessionKey"),
                request.getString("encryptedData"),
                request.getString("iv"));
    }

    /**
     * 获取用户信息
     *
     * @param request           请求参数
     * @return                  WxMaPhoneNumberInfo
     * @throws WxErrorException WxErrorException
     */
    @PostMapping(value = "/api/base/tx/user/getUserInfo", produces = "application/json;charset=UTF-8")
    public WxMaUserInfo getUserInfo(@RequestBody JSONObject request) throws WxErrorException {
        return wxMaUserService.getUserInfo(
                request.getString("sessionKey"),
                request.getString("encryptedData"),
                request.getString("iv"));
    }

    /**
     * 构造初始信息
     * @param wxMpService
     */
    public WeixinApp() {
        WxMaService wxMaService = new WxMaServiceImpl();
        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
        config.setAppid(appId);
        config.setSecret(appSecret);
        wxMaService.setWxMaConfig(config);
        wxMaUserService = new WxMaUserServiceImpl(wxMaService);

        final WxMpServiceImpl wxMpService = new WxMpServiceImpl();
        final WxMpDefaultConfigImpl wxConfigProvider = new WxMpDefaultConfigImpl();
        wxConfigProvider.setAppId(appId);
        wxConfigProvider.setSecret(appSecret);
        wxMpService.setWxMpConfigStorage(wxConfigProvider);
        this.wxMpService = wxMpService;
    }
}
