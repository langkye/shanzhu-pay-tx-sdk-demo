package com.shanzhu.pay.demo.simpleExample.refund;

import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.model.refund.CreateRefundRequest;
import com.shanzhu.pay.sdk.model.refund.CreateRefundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 申请退款
 * 接口文档：https://www.yuque.com/ycp2i6/yffxqb/nh3tqq
 *
 * @author langkye
 * @date 2021/9/6
 */
public class RefundChargeDemo {
    public static final String PAY_REFUND_URL = "https://ip:port/api/szszfin/payNotify";

    private static final Logger logger = LoggerFactory.getLogger(RefundChargeDemo.class);
    /**测试*/
    private static final String DEV_SERVER_URL = "https://apidev.szszfin.com";
    /**生产*/
    private static final String PROD_SERVER_URL = "https://api.szszfin.com";

    public static void main(String[] args) {
        /*1/：创建客户端配置*/
        DefaultShanZhuPayClient.Config config = new DefaultShanZhuPayClient.Config();
        config.setServerUrl(DEV_SERVER_URL);
        //商户号：从商户门户端获取
        config.setMchId("");
        //商户私钥：从商户门户端指引生成
        config.setPrivateKey("");
        //善筑平台公钥：从商户门户端获取
        config.setShanzhuPublicKey("");
        //商户API秘钥：从商户门户端获取
        config.setApiKey("");
        //秘钥序列号：从商户门户端获取
        config.setKeySerialNo("");

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        CreateRefundRequest request = new CreateRefundRequest();
        // 商户订单号：从下单接口返回
        request.setSzOrderNo("70501111111S001111119");
        // 订单总金额，单位为分。
        request.setTotalAmount(10000);
        //退款金额
        request.setRefundAmount(5000);
        //退款订单号
        request.setOutRefundNo("1217752501201407033233368018");
        //描述
        request.setDescription("商品已售完");
        // 通知地址,必须为直接可访问的URL，不允许携带查询串。
        request.setNotifyUrl(PAY_REFUND_URL);
        CreateRefundResponse response = shanZhuPayClient.createRefund(request);
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData() );
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getSubMsg());
        }
    }
}
