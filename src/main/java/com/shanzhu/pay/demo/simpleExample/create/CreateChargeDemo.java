package com.shanzhu.pay.demo.simpleExample.create;

import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.model.charge.CreateChargeRequest;
import com.shanzhu.pay.sdk.model.charge.CreateChargeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author langkye
 * @date 2021/10/15
 */
public class CreateChargeDemo {
    public static final String PAY_NOTIFY_URL = "https://ip:port/api/szszfin/payNotify";
    private static final Logger logger = LoggerFactory.getLogger(CreateChargeDemo.class);

    public static void main(String[] args) {
        DefaultShanZhuPayClient.Config config = new DefaultShanZhuPayClient.Config();
        config.setServerUrl("https://apidev.szszfin.com");
        config.setMchId(""); // 商户号
        config.setPrivateKey(""); // 商户私钥
        config.setShanzhuPublicKey(""); // 善筑平台公钥
        config.setApiKey(""); // 商户API秘钥
        config.setKeySerialNo(""); // 秘钥序列号
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        CreateChargeRequest request = new CreateChargeRequest();
        request.setOutOrderNo("70501111111S001111120"); // 商户订单号
        request.setChannel("SCAN");//扫码
        request.setAmount(1); // 订单总金额，单位为分。
        // 通知地址,必须为直接可访问的URL，不允许携带查询串。
        request.setNotifyUrl(PAY_NOTIFY_URL);
        Map<String, Object> extra = new HashMap<>();
        extra.put("auth_code", "285486956408781215");
        // 下面是可选参数，一般不用传
        request.setExtra(extra);
        CreateChargeResponse response = shanZhuPayClient.createCharge(request);
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData());
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getSubMsg());
        }
    }
}
