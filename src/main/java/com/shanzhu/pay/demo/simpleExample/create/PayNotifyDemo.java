package com.shanzhu.pay.demo.simpleExample.create;

import com.shanzhu.pay.demo.charge.consts.NotifyType;
import com.shanzhu.pay.demo.charge.model.NotifyResponse;
import com.shanzhu.pay.demo.charge.model.params.NotifyBizData;
import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.exception.InvalidNotificationException;
import com.shanzhu.pay.sdk.model.charge.ChargeNotificationData;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;

/**
 * 支付回调
 *
 * @author langkye
 * @date 2021/10/15
 */
public class PayNotifyDemo {
    private final Logger logger = LoggerFactory.getLogger(PayNotifyDemo.class);

    /**
     * 支付回调：回调地址为，下单时候送到"善筑"的地址
     * @param httpServletRequest HttpServletRequest
     * @return
     *      {"code":"SUCCESS","msg":"成功"}
     *      {"code":"FAILED","msg":"失败"}
     */
    @RequestMapping(CreateChargeDemo.PAY_NOTIFY_URL)
    public Object payNotify(HttpServletRequest httpServletRequest) {
        /*响应信息*/
        final NotifyResponse response = new NotifyResponse();

        /*从请求头获取参数*/
        final String authorization = httpServletRequest.getHeader("Authorization");
        final StringBuffer requestUrl = httpServletRequest.getRequestURL();
        final String method = httpServletRequest.getMethod();

        //解析参数：body
        String body = "";
        try {
            final InputStream inputStream = httpServletRequest.getInputStream();
            body = IOUtils.toString(inputStream);
            logger.info("###authorization: {}", authorization);
            logger.info("###body: {}", body);
            logger.info("###requestURL: {}", requestUrl.toString());
            logger.info("###method: {}", method);
        } catch (Exception ex) {
            logger.error("获取参数失败：{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            String message = "解析回调请求参数失败:" + ex.getMessage();
            response.setBizException(true);
            response.setClientCode("OTHER_ERROR");
            response.setClientMessage(message);
            return response;
        }

        /*开始构建[善筑SDK客户端]配置*/
        DefaultShanZhuPayClient.Config config = new DefaultShanZhuPayClient.Config();
        //API接口服务地址
        config.setServerUrl("https://apidev.szszfin.com");
        //商户号：善筑下发
        config.setMchId("");
        //商户私钥：善筑下发
        config.setPrivateKey("");
        //善筑平台公钥：善筑下发
        config.setShanzhuPublicKey("");
        //商户API秘钥：善筑下发
        config.setApiKey("");
        //秘钥序列号：善筑下发
        config.setKeySerialNo("");

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);

        //解析数据体
        try {
            final ChargeNotificationData chargeNotificationData = shanZhuPayClient
                    .checkAndParseChargeNotification(
                            method,
                            String.valueOf(requestUrl),
                            body,
                            authorization);
            //创建订单时的商户订单号
            final String outOrderNo = chargeNotificationData.getOutOrderNo();
            //支付状态：
            //      SUCCESS-支付成功；
            //      NOT_PAY-未支付；
            //      CLOSED-已关闭；
            //      ACCEPT-已接收，等待扣款；
            //      USER_PAYING-用户支付中；
            //      PAY_FAILED-支付失败
            final String status = chargeNotificationData.getStatus();

            // TODO 这里调用你的业务逻辑，设置合理的状态
            boolean success = true;

            /*构建响应信息*/
            if(success){
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.SUCCESS)
                                .msg(NotifyType.SUCCESS.getMsg())
                                .build());
            } else {
                response.setNotifyBizData(
                        NotifyBizData.builder()
                                .code(NotifyType.FAILED)
                                .msg(NotifyType.FAILED.getMsg())
                                .build());
            }
        } catch (InvalidNotificationException ex) {
            logger.error("校验通知失败:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("INVALID_NOTIFICATION");
            response.setClientMessage("无效的回调通知:" + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            logger.error("非法的参数:{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("ILLEGAL_ARGUMENT");
            response.setClientMessage("参数异常:" + ex.getMessage());
        } catch (Exception ex) {
            logger.error("未知异常：{}", ex.getMessage());
            //打印堆栈信息
            ex.printStackTrace();

            response.setBizException(true);
            response.setClientCode("OTHER_ERROR");
            response.setClientMessage("未知异常，回调处理失败:" + ex.getMessage());
        }
        return response;
    }
}
