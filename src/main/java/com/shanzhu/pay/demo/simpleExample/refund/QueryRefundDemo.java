package com.shanzhu.pay.demo.simpleExample.refund;

import com.shanzhu.pay.sdk.DefaultShanZhuPayClient;
import com.shanzhu.pay.sdk.model.refund.QueryRefundRequest;
import com.shanzhu.pay.sdk.model.refund.QueryRefundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询单笔退款
 * 接口文档：https://www.yuque.com/ycp2i6/yffxqb/hdgtgm
 *
 * @author langkye
 * @date 2021/9/6
 */
public class QueryRefundDemo {
    private static final Logger logger = LoggerFactory.getLogger(QueryRefundDemo.class);
    /**测试*/
    private static final String DEV_SERVER_URL = "https://apidev.szszfin.com";
    /**生产*/
    private static final String PROD_SERVER_URL = "https://api.szszfin.com";

    public static void main(String[] args) {
        /*1/：创建客户端配置*/
        DefaultShanZhuPayClient.Config config = new DefaultShanZhuPayClient.Config();
        config.setServerUrl(DEV_SERVER_URL);
        //商户号：从商户门户端获取
        config.setMchId("");
        //商户私钥：从商户门户端指引生成
        config.setPrivateKey("");
        //善筑平台公钥：从商户门户端获取
        config.setShanzhuPublicKey("");
        //商户API秘钥：从商户门户端获取
        config.setApiKey("");
        //秘钥序列号：从商户门户端获取
        config.setKeySerialNo("");

        /*2/：创建客户端*/
        DefaultShanZhuPayClient shanZhuPayClient = new DefaultShanZhuPayClient(config);
        QueryRefundRequest request = new QueryRefundRequest();
        //创建订单时返回的善筑支退款订单号，建议优先使用(二选一)
        request.setSzRefundNo("1217752501201407033233368018");
        //创建订单时的商户退款订单号(二选一)
        //request.setOutRefundNo("");

        QueryRefundResponse response = shanZhuPayClient.queryRefund(request);
        if (response.isSuccess()) {
            logger.info("调用成功, 返回数据：{}", response.getBizData() );
        } else if (response.isServiceError()) {
            logger.warn("调用失败，服务不可用，请重试");
        } else if (response.isBizError()) {
            logger.warn("调用失败，业务错误: {}", response.getSubMsg());
        }
    }
}
