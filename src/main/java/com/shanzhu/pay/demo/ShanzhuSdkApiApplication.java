package com.shanzhu.pay.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author langkye
 * @date 2021/9/9
 */
@SpringBootApplication
public class ShanzhuSdkApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShanzhuSdkApiApplication.class, args);
    }
}
