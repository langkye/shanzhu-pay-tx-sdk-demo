package com.shanzhu.pay.demo.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * @author luobin
 * @date 2020/11/30 19:05
 * @description
 */
public class PropertyUtil {

    @SuppressWarnings("rawtypes")
    private static Map map = null;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static void loadFile() {
        map = new HashMap();
        try {
            Properties p = new Properties();
            p.load(PropertyUtil.class.getClassLoader().getResourceAsStream("application.properties"));
            for (Object o : p.keySet()) {
                String key = (String) o;
                String value = p.getProperty(key);
                map.put(key, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getValue(String str) {
        if (map == null) {
            loadFile();
        }
        return (String) map.get(str);
    }
}
