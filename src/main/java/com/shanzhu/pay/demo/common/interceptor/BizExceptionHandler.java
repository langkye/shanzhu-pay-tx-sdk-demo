package com.shanzhu.pay.demo.common.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.shanzhu.pay.demo.common.exception.RRException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author zhangfengyang
 * @date 2019-03-26
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class BizExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(RRException.class)
    protected ResponseEntity<JSONObject> handleBusinessException(RRException ex) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", ex.getCode());
        jsonObject.put("isBusinessError", true);
        jsonObject.put("message", ex.getMessage());
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }
}

