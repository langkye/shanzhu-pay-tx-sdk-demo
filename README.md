> 善筑支付SDK DEMO
> 
> 接口文档地址: https://www.yuque.com/ycp2i6/yffxqb

### 概述
- 配置
  ![](./doc/img/config-1.png)
  ![](./doc/img/config-2.png)

- 目录结构
  ```markdown
  /shanzhu-pay-tx-sdk-demo
        |__lib
        |  |__支付SDK 2.0.0: shanzhu-pay-tx-sdk-2.0.0.jar
        |  |__支付SDK 2.0.1: shanzhu-pay-tx-sdk-2.0.1.jar（新增支付分账功能）
        |__src/main
        |    |__java/com/shanzhu/pay/demo
        |    |  |__applets
        |    |  |  |__controller/WeixinApp（微信小程序）
        |    |  |__charge（交易）
        |    |  |  |__consts（常量定义）
        |    |  |  |__controller（Restful API）
        |    |  |  |__model
        |    |  |  |__service
        |    |  |  |  |__IApiPayService.java（物业处理）
        |    |  |  |  |__ITxShanzhuSdkApiService.java（SDK接口分装）
        |    |  |  |  |__impl（接口实现）
        |    |  |__common（公共：异常、拦截器、工具）
        |    |  |__config（配置类）
        |    |  |__ShanzhuSdkApiApplication.java
        |    |__resources
        |       |__application.properties
        |__pom.xml
        |__README.md
  ```

### TX API交易接口
> 交易API入口:`com.shanzhu.pay.demo.charge.controller.PayApiController`

> Simple Example（简单示例main方法调用）: `com/shanzhu/pay/demo/simpleExample`  

### Other API其他接口
> 小程序API入口:`com.shanzhu.pay.demo.applets.controller.WeixinApp`

